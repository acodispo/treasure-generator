<?php
$treasures = [
   "H" => [
      "cp" => [
         33,
         rand(1,20)*1000,
      ],
      "sp" => [
         50,
         rand(1,100)*1000,
      ],
      "gp" => [
         83,
         (rand(1,12)+rand(1,12)+rand(1,12)+rand(1,12))*1000,
      ],
      "gems" => [
         50,
         rand(1,100),
      ],
      "jewels" => [
         50,
         rand(1,20)+rand(1,20),
      ],
   ],
   "D" => [
      "cp" => [
         17,
         rand(1,4)*1000,
      ],
      "sp" => [
         17,
         rand(1,10)*1000,
      ],
      "gp" => [
         67,
         rand(1,6)*1000,
      ],
      "gems" => [
         33,
         rand(1,8),
      ],
      "jewels" => [
         33,
         rand(1,2),
      ],
   ],
];

$type="D";

foreach ($treasures[$type] as $k => $v) {
   if (rand(1,100)<$v[0]) {
      $treasure[$k]['number'] = $v[1];
      if ($k == 'cp') {
         $treasure[$k]['weight'] = ceil($v[1]/20);
         $treasure[$k]['slots'] = ceil($v[1]/100);
         $treasure[$k]['total value'] = $v[1]/100;
      } elseif ($k == 'sp') {
         $treasure[$k]['weight'] = ceil($v[1]/20);
         $treasure[$k]['slots'] = ceil($v[1]/100);
         $treasure[$k]['total value'] = $v[1]/10;
      } elseif ($k == 'gp') {
         $treasure[$k]['weight'] = ceil($v[1]/20);
         $treasure[$k]['slots'] = ceil($v[1]/100);
         $treasure[$k]['total value'] = $v[1];
      } elseif ($k == 'gems') {
         $treasure[$k]['weight'] = ceil($v[1]/10);
         $treasure[$k]['slots'] = ceil($v[1]/50);
         $i = $treasure[$k]['number'];
         while ($i>0) {
            $i--;
            $r = rand(1,100);
            if ($r>=1 && $r<=8) {
               $items[] = 10;
            } elseif ($r>=9 && $r<=22) {
               $items[] = 30;
            } elseif ($r>=23 && $r<=67) {
               $items[] = 100;
            } elseif ($r>=68 && $r<=87) {
               $items[] = 300;
            } elseif ($r>=88 && $r<=98) {
               $items[] = 1000;
            } elseif ($r==99) {
               $items[] = 3000;
            } elseif ($r==100) {
               $items[] = 10000;
            }
         }
         $treasure[$k]['total value'] = array_sum($items);
         $treasure[$k]['items'] = array_count_values($items);
         ksort($treasure[$k]['items']);
         unset($items);
      } elseif ($k == 'jewels') {
         $treasure[$k]['weight'] = ceil($v[1]/10);
         $treasure[$k]['slots'] = ceil($v[1]/50);
         $i = $treasure[$k]['number'];
         while ($i>0) {
            $i--;
            $r = rand(1,100);
            if ($r>=1 && $r<=20) {
               $items[] = (rand(1,6)+rand(1,6))*100;
            } elseif ($r>=21 && $r<=40) {
               $items[] = (rand(1,6)+rand(1,6)+rand(1,6))*100;
            } elseif ($r>=41 && $r<=60) {
               $items[] = (rand(1,10)+rand(1,10)+rand(1,10)+rand(1,10))*100;
            } elseif ($r>=61 && $r<=80) {
               $items[] = rand(1,6)*1000;
            } elseif ($r>=81 && $r<=90) {
               $items[] = (rand(1,4)+rand(1,4))*1000;
            } elseif ($r>=91 && $r<=100) {
               $items[] = (rand(1,6)+rand(1,6))*1000;
            }
         }
         $treasure[$k]['total value'] = array_sum($items);
         $treasure[$k]['items'] = array_count_values($items);
         ksort($treasure[$k]['items']);
         unset($items);
      } elseif ($k == 'maps') {
         $treasure[$k]['weight'] = ceil($v[1]*5);
         $treasure[$k]['slots'] = ceil($v[1]);
         $i = $treasure[$k]['number'];
         while ($i>0) {
            $i--;
            $r = rand(1,100);
            if ($r>=1 && $r<=50) {
               $items[] = "Treasure map";
            } elseif ($r>=51 && $r<=85) {
               $items[] = "Magic item map";
            } elseif ($r>=86 && $r<=100) {
               $items[] = "Treasure & magic item map";
            }
         }
         $treasure[$k]['items'] = array_count_values($items);
         ksort($treasure[$k]['items']);
         unset($items);
      } elseif ($k == 'magic') {
         // 
         $treasure[$k]['weight'] = ceil($v[1]/20);
         $treasure[$k]['slots'] = ceil($v[1]/500);
         $treasure[$k]['total value'] = $v[1];
      }
   }
}

// display
echo '
<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" href="data:image/png;base64,">
<title>Treasure Generator</title>
<style>
   body{
      margin:1em auto;
      max-width:40em;
      padding:0 .62em;
      font:1.2em/1.62 sans-serif;
   }
   h1,h2,h3 {
      line-height:1.2;
   }
   @media print{
      body{
         max-width:none
      }
   }
</style>
<article>
<header>
<h1>Here is a Type '.$type.' Treasure</h1>
</header>
';
if (isset($treasure)) {
   echo '<table>
      <thead>
      <tr>
      <th>Qty</th>
      <th>Name/Description</th>
      <th>Value Each</th>
      <th>Value Total</th>
      <th>Weight (lbs)</th>
      <th>Slots</th>
      </tr>
      </thead>
      <tbody>';
   foreach ($treasure as $k => $v) {
      echo '<tr>';
      echo '<td>'.$v['number'].'</td>';
      echo '<td>'.$k.'</td>';
      echo '<td></td>';
      echo '<td>'.$v['total value'].'</td>';
      echo '<td>'.$v['weight'].'</td>';
      echo '<td>'.$v['slots'].'</td>';
      echo '</tr>';
   }
   echo '
         </tbody>
         </table>';
   echo '<pre>';
   print_r($treasure);
   echo '</pre>';
} else {
   echo '<b>No treasure!</b>';
}
echo '
</article>
';
